package com.pythonwiki.web.controller.admin;

import com.pythonwiki.model.domain.Options;
import com.pythonwiki.model.dto.JsonResult;
import com.pythonwiki.model.dto.PythonwikiConst;
import com.pythonwiki.model.enums.PythonwikiEnums;
import com.pythonwiki.service.OptionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author : 宋浩志
 * @createDate : 2018年10月12日
 */

@Controller
@RequestMapping("/admin/option")
public class OptionsController extends BaseController {
	@Autowired
	private OptionsService optionsService;

	/**
	 * 所有设置选项
	 *
	 * @param model
	 * @return
	 */
	@GetMapping
	public String option(Model model) {
		return "admin/admin_options";
	}

	/**
	 * 保存设置
	 *
	 * @param map
	 * @return
	 */
	@PostMapping(value = "/save")
	@ResponseBody
	public JsonResult save(@RequestParam Map<String, String> map) {
		try {
			optionsService.save(map);
			PythonwikiConst.OPTIONS.clear();
			List<Options> listMap = optionsService.selectMap();
			for (Options options : listMap) {
				PythonwikiConst.OPTIONS.put(options.getOptionName(), options.getOptionValue());
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			return new JsonResult(PythonwikiEnums.PRESERVE_ERROR.isFlag(), PythonwikiEnums.PRESERVE_ERROR.getMessage());
		}
		return new JsonResult(PythonwikiEnums.PRESERVE_SUCCESS.isFlag(), PythonwikiEnums.PRESERVE_SUCCESS.getMessage());
	}
}
