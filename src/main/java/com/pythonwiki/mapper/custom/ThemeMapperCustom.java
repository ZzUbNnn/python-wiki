package com.pythonwiki.mapper.custom;

import org.apache.ibatis.annotations.Param;

/**
 * @author: zb
 * @createTime: 2021-06-20
 */
public interface ThemeMapperCustom {

	void updateStatus(@Param(value="status")int status,@Param(value="id") int id);

}
