package com.pythonwiki.service;

import com.pythonwiki.model.domain.Options;

import java.util.List;
import java.util.Map;

/**
 * @author: zb
 * @createTime: 2021-06-20
 */
public interface OptionsService {
	/**
	 * 保存多个
	 * 
	 * @param map
	 */
	void save(Map<String, String> map) throws Exception;

	/**
	 * 所有设置选项
	 * 
	 * @return
	 */
	List<Options> selectMap();

	/**
	 * 保存单个
	 * 
	 * @param key
	 * @param value
	 */
	void saveOption(String key, String value);

	/**
	 * 删除
	 * 
	 * @param options
	 */
	void delete(Options options) throws Exception;

}
