package com.pythonwiki;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.pythonwiki.mapper")
public class PythonWikiApplication {

    public static void main(String[] args) {
        SpringApplication.run(PythonWikiApplication.class, args);
    }

}
